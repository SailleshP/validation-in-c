﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ValidationSample
{
    /// <summary>
    /// Problem with this approach if something change the whole dll has too be deployed and only one validation works rest not.
    /// </summary>

    public class User
    {
        [MaxWords(1)]
        public string FirstName { get; set; }

        [MaxWords(1)]
        public string Address { get; set; }
    }

    public class Shipper : IValidatableObject
    {
        public string AccountNumber { get; set; }

        public string BillingAccountNumber { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (this.Address1.Length > 35)
            {
                yield return new ValidationResult("Address should be less than 35 characters", new List<string>() { "Address" });
            }
        }
    }
}