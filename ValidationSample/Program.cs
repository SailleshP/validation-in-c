﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidationSample
{
    public class MaxWordsAttribute : ValidationAttribute
    {
        private readonly int _maxwords;

        public MaxWordsAttribute(int maxWords)
            : base("{0} has to many words")
        {
            _maxwords = maxWords;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var valueAsString = value?.ToString();
            if (valueAsString?.Split(' ').Length > _maxwords)
            {
                var errorMessages = FormatErrorMessage(validationContext.DisplayName);
                return new ValidationResult(errorMessages);
            }
            return ValidationResult.Success;
        }
    }

    internal class Program
    {
        private static void Main(string[] args)
        {
            Shipper shipper = new Shipper();
            shipper.Address1 = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            shipper.AccountNumber = "1231231";
            shipper.Address2 =
                "assssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";

            ValidationContext context = new ValidationContext(shipper, null, null);
            List<ValidationResult> results = new List<ValidationResult>();
            bool valid = Validator.TryValidateObject(shipper, context, results, true);
            if (!valid)
            {
                foreach (ValidationResult vr in results)
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("Member Name :{0}", vr.MemberNames.First());
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("   ::  {0}{1}", vr.ErrorMessage, Environment.NewLine);
                }
            }

            //#region UserVALIDATION
            //User user = new User();
            //try
            //{
            //    user.Address = "ajabpur khurd ganhes vihar b dehradun  adssa          asdasdasada";
            //    user.FirstName = "saillesh pawarar";

            //    ValidationContext context = new ValidationContext(user, null, null)
            //    {
            //        DisplayName = "Customer Address",
            //    };
            //    List<ValidationResult> results = new List<ValidationResult>();
            //    bool valid = Validator.TryValidateObject(user, context, results, true);

            //    if (!valid)
            //    {
            //        foreach (ValidationResult vr in results)
            //        {
            //            Console.ForegroundColor = ConsoleColor.Blue;
            //            Console.Write("Member Name :{0}", vr.MemberNames.First());
            //            Console.ForegroundColor = ConsoleColor.Red;
            //            Console.Write("   ::  {0}{1}", vr.ErrorMessage, Environment.NewLine);
            //        }
            //    }

            //    Console.ForegroundColor = ConsoleColor.White;
            //    Console.WriteLine("\nPress any key to exit");
            //    Console.ReadKey();

            //    //only for one property
            //    //var results = new List<ValidationResult>();
            //    //var vc = new ValidationContext(user, null, null)
            //    //{ MemberName = "Address", DisplayName = "Customer Address" };
            //    //var isValid = Validator.TryValidateProperty(user.Address, vc, results);
            //    //var errors = Array.ConvertAll(results.ToArray(), o => o.ErrorMessage);
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message.ToString());
            //}

            //Console.Read();
            //#endregion
        }
    }
}